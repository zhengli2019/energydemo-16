''' Prepossess EU-CORDEX
    --------------------
    Outline a script to merge and subset CORDEX files aquired 
    through provided downloading scripts.                            

    The metprms considered are assigned by arguments so use for example:
    `$ python precproc-cordex.py uas vas`

    * Assumes a certain directory structure. Edit the `datadir` variable 
      if this doesn't fit.                                                  '''

import glob as grod
import numpy as np
import sys
import os

from cdo import *
cdo = Cdo()

data_product = 'eucordex'                # choose data product
region_lab = 'NorthSea'                  # choose sub-region
metprms = sys.argv[1:]                   # use externally set fields

''' Set variables
    -------------
    Here set some values so that can make meta choices to describe what
    regions etc, that we want to deal with, and hold the variables these
    imply in the following tables/variables. 
    * Set a dictionary that matches a region label to a bounding box.
      Box is described as a string with form that matches syntax for 
      the cdo command `selonlatbox`:
      `[MIN-LON],[MAX-LON],[MIN-LAT],[MAX-LAT]`                            '''

region_boxes = {'NorthSea' : '-10,12,49,62'}
product_keys = {'eucordex' : 'EUR-11'}

# ====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====

''' Find data files
    ---------------
    Go to the specified directory and list out the CORDEX files contained
    within. Do this using windcards (*) with the assumption that the files
    are in NetCDF format (.nc).                                            '''

datadir = '/home/ubuntu/'+data_product
print(datadir)
os.chdir(datadir)
filenames = []
if data_product == 'eucordex' :
    if metprms :
        for metprm in metprms :
            filenames += grod.glob(
                metprm+'*'+product_keys[data_product]+'*.nc')
    else :
        print('*'+product_keys[data_product]+'*.nc')
        grod.glob(
            '*'+product_keys[data_product]+'*.nc')
else :
    print("+++ Unknown data product +++")

filenames = np.array(filenames)

''' Process files
    -------------
    * Find 'set' of simulations, using the same models, over the same period,
      assuming the same scenario, brocken up by time intervals. 
    * Subset these spatially to the region of interest.                   
    * Merge these sperate time slices into one file.                     
    * Aggrigate as appropriate for given                                    
    * Discard the original files to save space                              '''

if not os.path.isdir(region_lab) :                 # create storage directory
    print(
        "* Creating storage directory: "
        +region_lab)
    os.mkdir(region_lab)
    
headers = np.array(['_'.join(x.split('_')[:-1]) for x in filenames])
for sim in np.unique(headers) :
    print("* Processing : "+sim)
    ix = (sim == headers)
    count = 0 
    for filename in filenames[ix] :
        cdo.sellonlatbox(                          # select region
            region_boxes[region_lab],input=filename,
            output='tmp'+str(count)+'.nc')
        count += 1

    subsets = grod.glob('tmp*.nc')
    cdo.mergetime(                                 # merge times
        input=' '.join(subsets),
        output=region_lab+'/'+region_lab+'_'+sim+'.nc')

    for subset in subsets :                        
        os.remove(subset)                          # tidy tmp files

    ## Aggregations 
    metprm = sim.split('_')[0]
    if metprm == 'pr' :
        cdo.monsum(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monsum_'+region_lab+'_'+sim+'.nc')
        cdo.yearmax(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/yearmax_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')
    if metprm in ['uas','vas'] : 
        filenames_vas = (
            region_lab+'/'+region_lab+'_'
            +'vas_'+'_'.join(sim.split('_')[1:])+'.nc')
        filenames_uas = (
            region_lab+'/'+region_lab+'_'
            +'uas_'+'_'.join(sim.split('_')[1:])+'.nc')
        if ((os.path.isfile(filenames_vas) == True)
            and
            (os.path.isfile(filenames_uas)==True)):
            cdo.merge(
                input =" ".join((filenames_vas,filenames_uas)),
                output='tmp.nc')
            cdo.expr(
                '"sfcWind=sqrt(vas*vas+uas*uas)"',
                input='tmp.nc',
                output='tmp01.nc')
            cdo.monmean(
                input = 'tmp01.nc',
                output= region_lab+'/monmean_'+region_lab+'_'+'sfcWind_'+
                '_'.join(sim.split('_')[:])+'.nc')
            [os.remove(x) for x in ['tmp.nc','tmp01.nc']]
    if metprm in ['tasmax'] : 
        cdo.monmax(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monmax_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')
    if metprm in ['clt'] : 
        cdo.monmean(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monmean_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')
    if metprm in ['rsds'] : 
        cdo.monmean(
            input=region_lab+'/'+region_lab+'_'+sim+'.nc',
            output=region_lab+'/monmean_'+region_lab+'_'+sim+'.nc')
        os.remove(region_lab+'/'+region_lab+'_'+sim+'.nc')


# ====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====:<>:====

# ____________________________________________________________________________
# ////////////////////////////////////////////////////////////////////////////

''' Sometimes run the above script and then later want perform a
    further aggrigation, but have already discarded the original
    downloads to conserve disk space. Current work around is to have
    reserved metprm name that describes an aggrigation and then apply
    the commands below. 

    ** Maybe in future rather than doing the aggrigations and then
       discarding the files that have then been condensed, could do
       the condensing and regional sub-setting in one pass and then do
       other processing in another pass? Would depend on how much disk
       space is assumed to be avalible and what/how we want to archive.      '''

if 'sfcWind' in metprms:
    filenames_vas = []
    filenames_uas = []
    filenames_vas += grod.glob(
        datadir+'/'+region_lab+'/'+region_lab
        +'_vas'+'*'+product_keys[data_product]+'*.nc')
    filenames_uas += grod.glob(
        datadir+'/'+region_lab+'/'+region_lab
        +'_uas'+'*'+product_keys[data_product]+'*.nc')
    filenames_vas.sort()
    filenames_uas.sort()
    filenames_vas = np.array(filenames_vas)         
    headers_vas = np.array(['_'.join(x.split('_')[2:]) for x in filenames_vas])
    filenames_uas = np.array(filenames_uas)         
    headers_uas= np.array(['_'.join(x.split('_')[2:]) for x in filenames_uas])
    idx = 0    
    while idx < len(filenames_vas):
        if headers_vas[idx]==headers_uas[idx]: # test if the data match
            cdo.merge(
                input =" ".join((filenames_vas[idx],filenames_uas[idx])),
                output='tmp.nc')
            cdo.expr(
                '"sfcWind=sqrt(vas*vas+uas*uas)"',
                input='tmp.nc',
                output='tmp01.nc')
            cdo.monmean(
                input='tmp01.nc',
                output=(
                    region_lab+'/monmean_'+region_lab+'_'
                    +'sfcWind'+'_'+headers_vas[idx]+'.nc'))
            [os.remove(x) for x in ['tmp.nc','tmp01.nc']]
            idx +=1

        else:
            print ('select wrong data')
            continue
            idx +=1

            

